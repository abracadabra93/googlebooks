<?php

function getFiles(){
	// $path = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'book';
	// $path = 'G:\COMPUTER SCIENCE\DOC IN CHINESE';
	$path = 'G:\COMPUTER SCIENCE\DOC IN ENGLISH';
	$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
	$objects->setFlags(FilesystemIterator::SKIP_DOTS);

	$other = 0;
	$output = array();
	$type = array();
	$count = 1;
	foreach($objects as $key=>$obj){
		if($obj->isFile()){
			$filename = str_encode(basename($key));
			$filestat = pathinfo($filename[0]);
    		$path = str_encode($obj->getPath());
    		$type_filter = array('pdf','epub','chm','mobi');
    		if(isset($filestat['extension']) && in_array(strtolower($filestat['extension']),$type_filter)){
				$output[] = array("no"=>$count, 'path'=>$path[0],'filename'=>$filename[0],'encoding'=>$filename[1],'info'=>$filestat);
				$count ++;
			}elseif(isset($filestat['extension']) && $filestat['extension'] != ''){
				array_search($filestat['extension'],$type) === false && $type[] = $filestat['extension'];
				$other += 1;
			}else{
				$other += 1;
			}
		}

	}
	$objects->endIteration();
	return array('books'=>$output,'other_file'=>array('num'=>$other,'type'=>$type));

}

function str_encode($str)
{
	mb_detect_order('UTF-8,CP936');
	$current_encoding = mb_detect_encoding($str);
    $encoded_text = iconv($current_encoding, 'UTF-8', $str);
    return array($encoded_text,$current_encoding);

}


function fmp($input){//print formatted result;
	echo '<pre>';
	print_r($input);
	echo '</pre>';
}


function db_handler()
{
	$mysqli = new mysqli("localhost", "root", "", "google");
	if ($mysqli->connect_errno) {
    	return "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}
	$mysqli->set_charset('utf8');
	return $mysqli;
}

function insert_data($handler,$query){
	return $handler->query($query);
}


function exec_cmd(){
	chdir('C:\cli\xpdfbin\bin64');
	$cmd =  'pdfinfo -f 1 -l 4 ' . '"' . "F:\WAMP64\www\googlebook\book\Learn By Doing Quick Guide to J - Jay'Neisha Jackson.pdf" . '"';
	return shell_exec($cmd);

}

function insertQueryBuilder($books){
	$handler = db_handler();
	define('TIMESTAMP',time());
	$query_header = "INSERT INTO `books` (`filename`,`path`,`type`,`name`,`encoding`,`timestamp`) VALUES ";
	$query = '';
	$count = 0;
	foreach($books['books'] as $value){
		$count++;
		$path = $handler->real_escape_string($value['path']);
		$filename = $handler->real_escape_string($value['info']['filename']);
		$name = $handler->real_escape_string($value['filename']);
		$values = array($filename,$path, $value['info']['extension'],$name,$value['encoding'],TIMESTAMP);
		$str = implode('","',$values);
		$query .= '("' . $str . '"),';
		if($count == 20){
			$query = $query_header . $query;
			$query = mb_substr($query,0,-1);
			insert_data($handler,$query);
			$query = '';
			$count = 0;
		}
	}
	if($query != ""){
		$query = $query_header . $query;
		$query = mb_substr($query,0,-1);
		insert_data($handler,$query);
	}
	$handler->close();
	return true;
}




$books = getFiles();
fmp($books);
// insertQueryBuilder($books);
