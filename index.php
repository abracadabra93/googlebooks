<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Google books</title>

    <!-- Bootstrap core CSS -->
    <link href="http://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="style.css" rel="stylesheet">
  </head>

  <body>

    <div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-sm-12">
				<div class="panel panel-default">
				  <!-- Default panel contents -->
				  <div class="panel-heading">Panel heading</div>
				  <div class="panel-body">
					<p>Google books API</p>
					<div class="col-lg-6 col-sm-6">
						<div class="input-group">
							<input type="text" id="keyword" class="form-control" placeholder="Search for...">
							<span class="input-group-btn">
								<button class="btn btn-default" type="button" id="search" onclick="bookSearch();">Search</button>
							</span>
						</div><!-- /input-group -->
					</div>
					<div class="col-lg-6 col-sm-6">
						<span>Number of found Record: <b id="nfr"></b></span>
					</div>
				  </div>
				  <div class="col-lg-4 bordered-gbs">
				  	<table class="table"> 
					  <thead> 
						  <tr> 
							<th>#</th>
							<th>Title</th>
							<th>MetaData</th>
							<th>cover</th>
						  </tr>
					  </thead>
					  <tbody>
					  		<tr>
					  			<td>x</td>
					  			<td>x</td>
					  			<td>x</td>
					  			<td>x</td>
					  		</tr>
					  </tbody>
					  </table>
				  </div>
				  <!-- Table -->
				  <div class="col-lg-8">
				  <table class="table"> 
					  <thead> 
						  <tr> 
							<th>#</th>
							<th>Title</th> 
							<th>Authors</th> 
							<th>Publisher</th>
							<th>industryIdentifiers</th>
							<th>PageCount</th>
							<th>Categories</th>
							<th>Cover</th>
						  </tr>
					  </thead>
					  <tbody id="resultSet"> 
						<tr>
							<td colspan="9" style="text-align: center">Search books on Google Books</td>
						</tr>
					  </tbody> 
					  <tfoot>
						<tr>
						  <td  colspan="9" style="text-align: center">
						  	<nav aria-label="...">
							    <ul class="pagination" id="paginationBar">
							        <!-- <li class="disabled"><a href="#" aria-label="Previous"><span aria-hidden="true">«</span></a></li>
							        <li class="active">
							        	<a href="#">1 <span class="sr-only">(current)</span></a>
							        </li>
							        <li><a href="javascript(0);">2</a></li>
							        <li><a href="#">3</a></li>
							        <li><a href="#">4</a></li>
							        <li><a href="#">5</a></li>
							        <li ><a href="#" aria-label="Next"><span aria-hidden="true">»</span></a></li> -->
							    </ul>
							</nav>
						  </td>
						</tr>
					  </tfoot>
				  </table>
				  </div>
				</div>
			</div>
		</div>
    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="api.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
  </body>
</html>
