var startIndex = 0;
var maxResults = 10;

function bookSearch(offset =startIndex, numberResult = maxResults){
	var keyword = document.getElementById("keyword").value;
	var url = 'https://www.googleapis.com/books/v1/volumes?q=';
	$.ajax({
		url:url + keyword + '&startIndex=' + offset + '&maxResults=' + numberResult,
		dataType: "json",
		success: function(data){

			console.log(data);
			var resultSet = '';
			var pageBar =  pagination(data.totalItems, offset, maxResults); //generate page buttons
			document.getElementById('paginationBar').innerHTML = pageBar; //insert page buttons to tfoot
			document.getElementById('nfr').innerText = data.totalItems ; //insert page buttons to tfoot
			if(data.items == undefined || data.totalItems == 0){
				resultSet += '<td colspan="9" style="text-align: center">'  + keyword +' not found on Google Books</td>'
				document.getElementById('resultSet').innerHTML = resultSet;
			}else{
				for(var i=0; i < data.items.length; i++){
					var volumeInfo = data.items[i].volumeInfo;
					var pageNum = i + 1
					resultSet += '<tr><th scope="row">' + pageNum + '</th>'
					resultSet +=  '<td>' + volumeInfo.title + '</td>'
					resultSet +=  '<td>' + volumeInfo.authors + '</td>'
					resultSet +=  '<td>' + volumeInfo.publisher + '</td>'
					if(volumeInfo.industryIdentifiers != undefined && volumeInfo.industryIdentifiers.length > 0){
						resultSet += '<td>';
						var industryIdentifiers = volumeInfo.industryIdentifiers;
						for(var e=0;e<industryIdentifiers.length;e++){
						resultSet += industryIdentifiers[e].type + ': ' + industryIdentifiers[e].identifier + '<br>';
						}
						resultSet +=  '</td>';
					}else{
						resultSet += '<td>No record</td>';
					}
					resultSet +=  '<td>' + volumeInfo.pageCount + '</td>'
					resultSet +=  '<td>' + volumeInfo.categories + '</td>'
					if(volumeInfo.imageLinks != undefined && volumeInfo.imageLinks.smallThumbnail != undefined){
						resultSet +=  '<td> <a href="#"><img src="' + volumeInfo.imageLinks.smallThumbnail + '"> </a></td>'
					}else{
						resultSet +=  '<td> Result not found</td>'
					}
					resultSet += '</tr>'
				}
				document.getElementById('resultSet').innerHTML = resultSet;
			}
		},
		type: 'GET'
	});
}

function pagination(total, current, each = maxResults){
	var pageTotal = Math.ceil((total - startIndex) /10);
	var pageBtn = 5;
	var pageBtnSelected = current / 10; 
	var pageStart = pageBtnSelected - 2 > 0?pageBtnSelected - 2:0;
	var prev = pageBtnSelected == 0?0:current - 10;
	var next = current + 10;
	var pageBar = '<li class=""><a href="javascript:void(0);" onclick="bookSearch('  + prev + ');" aria-label="Previous"><span aria-hidden="true">«</span></a></li>'
	if(pageTotal > 1){
			for(var i = pageStart; i < pageBtn + pageStart; i++){
					var pageNum =  i + 1;
					var activeTag = pageBtnSelected == i?"active":"";
					var activeSpan = pageBtnSelected == i?'<span class="sr-only">(current)</span>':'';
					pageBar += '<li class="' + activeTag + '"><a href="javascript:void(0);" onclick="bookSearch('  + i * 10 + ');">'+ pageNum + activeSpan +'</a></li>';

			}
	}else{
		pageBar += '<li class="active"><a href="javascript:void(0);">1 <span class="sr-only">(current)</span></a></li>';
	}
	pageBar	+= '<li class=""><a href="javascript:void(0);" onclick="bookSearch('  + next + ');" aria-label="Next"><span aria-hidden="true">»</span></a></li>';
	//console.log(pageBar);
	return pageBar;
}
